Kreiranje tabela:

drop table if exists drzava;

create table drzava (
    kod varchar (3) not null,
    naziv varchar (45),
    primary key (kod)
);

drop table if exists grad;

create table grad (
	idGrad int (11) auto_increment not null,
    pttBroj varchar (10),
    naziv varchar (45),
    drzavaID varchar (3),
    primary key (idGrad),
    foreign key (drzavaID) references drzava(kod)
);

drop table if exists proizvodjac;

create table proizvodjac (
	idProizvodjac int auto_increment not null,
    naziv varchar (45),
    gradID int (11) not null,
    primary key (idProizvodjac),
    foreign key (gradID) references grad (idGrad)
);

drop table if exists kategorija;

create table kategorija (
	idKategorija int (11) auto_increment not null,
    naziv varchar (255),
    primary key (idKategorija)
);

drop table if exists proizvod;

create table proizvod (
	idProizvod int (11) auto_increment not null,
    naziv varchar (45),
    barkod varchar (13),
    proizvodjacID int (11),
    kategorijaID int (11),
    primary key (idProizvod),
    foreign key (proizvodjacID) references proizvodjac(idProizvodjac),
    foreign key (kategorijaID) references kategorija(idKategorija)
);

drop table if exists prodavnica;

create table prodavnica (
	idProdavnica int (11) auto_increment not null,
    naziv varchar (45),
    gradID int (11),
    primary key (idProdavnica),
    foreign key (gradID) references grad(idGrad)
);

drop table if exists proizvod_prodavnica;

create table proizvod_prodavnica (
	idPP int (11) auto_increment not null,
    proizvodID integer (11),
    prodavnicaID integer (11),
    primary key (idPP),
    foreign key (proizvodID) references proizvod(idProizvod),
    foreign key (prodavnicaID) references prodavnica(idProdavnica)
);


#1. Listu svih proizvoda, sortiranu po zemlji iz koje dolaze

RESENJE:
SELECT
	proizvod.naziv,
   	drzava.kod
FROM proizvod
JOIN proizvodjac
	ON proizvodjac.idProizvodjac = proizvod.proizvodjacID
JOIN grad
	ON proizvodjac.gradID = grad.idGrad
JOIN drzava
	ON grad.drzavaID = drzava.kod
order by drzava.kod


#2. Broj proizvoda koje pojedinačni proizvođači proizvode

RESENJE:
SELECT
	count(idProizvod),
   	proizvodjac.naziv
FROM proizvod
JOIN proizvodjac	
	ON proizvodjac.idProizvodjac = proizvod.proizvodjacID
group by proizvodjac.naziv


#3. Listu svih prodavnica iz određenog grada (u klauzuli treba da unesete naziv grada za
#koji želite prikaz)

SELECT 
	prodavnica.naziv,
    grad.naziv
FROM prodavnica
JOIN grad
	ON grad.idGrad = prodavnica.gradID
WHERE grad.naziv = 'Sombor'


#4. Listu svih proizvoda koji se nalaze u ponudi određene prodavnice.

SELECT 
	proizvod.naziv,
    prodavnica.naziv
FROM proizvod_prodavnica
JOIN proizvod
	ON proizvod.idProizvod = proizvod_prodavnica.proizvodID
JOIN prodavnica
	ON prodavnica.idProdavnica = proizvod_prodavnica.prodavnicaID
WHERE prodavnica.naziv = 'Market1'
    

#5. Listu proizvoda u određenoj kategoriji

SELECT 
	proizvod.naziv,
    kategorija.naziv
FROM proizvod
JOIN kategorija
	ON proizvod.kategorijaID = kategorija.idKategorija
WHERE kategorija.naziv = 'Dugotrajno mleko'


#6. Broj proizvoda po kategorijama.

SELECT
	count(idProizvod),
    kategorija.naziv
FROM proizvod
JOIN kategorija	
	ON kategorija.idKategorija = proizvod.kategorijaID
group by kategorija.naziv

